#!/bin/bash

# Version: 2023.07.31


set -o nounset
#set -o errexit
#set -o noclobber
#set -o noglob
set -x


# Variables
export USER_NAME="mastodon"


# stop containers
docker stop $(docker ps -qa | xargs)

# remove containers
docker rm $(docker ps -qa | xargs)
docker rmi $(docker images | tail -n +2| awk -F" " '{print $3}' | xargs)

# remove network
docker network rm $(docker network ls | grep containers | cut -d " " -f1)

# rm dirs in home
rm -rf /home/${USER_NAME}/postgres
rm -rf /home/${USER_NAME}/redis
rm -rf /home/${USER_NAME}/mastodon
rm -rf /home/${USER_NAME}/nginx

userdel ${USER_NAME}
rm -rf /home/${USER_NAME}

exit 0
