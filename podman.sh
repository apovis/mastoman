#!/bin/bash -x

# Version: 2023.07.31

# kudus goes to
# https://i12bretro.github.io/tutorials/0853.html


set -o nounset
set -o errexit
#set -o noclobber
#set -o noglob


# Variables
export USER_NAME="mastodon"
export WEB_DOMAIN="mastodon.your.domain"
NGINX_LOG_FILE="mastodon"

export USER01="roleowner"
export USER01_EMAIL="roleusr01@owner.com"
export ADMIN01="roleadmin"
export ADMIN01_EMAIL="roleadm01@admin.com"



install_podman() {
  # install prerequisites
  apt-get install -y podman podman-compose
}


create_mastodon_user() {
  useradd -U -m -s /bin/bash mastodon
  usermod --add-subuids 100000-165535 --add-subgids 100000-165535 ${USER_NAME}
}


fs_hierarchy() {
  # create working directories
  mkdir -p /home/${USER_NAME}/postgres 
  mkdir -p /home/${USER_NAME}/redis 
  mkdir -p /home/${USER_NAME}/mastodon/public/system
  mkdir -p /home/${USER_NAME}/nginx/conf
  mkdir -p /home/${USER_NAME}/nginx/logs
 
  # pull the mastodon web container
  podman pull tootsuite/mastodon
  
  # generate secrets, run this 2 times
  export MASTODON_SECRET_01=$(podman run --rm -it tootsuite/mastodon bundle exec rake secret)
  export MASTODON_SECRET_02=$(podman run --rm -it tootsuite/mastodon bundle exec rake secret)
  
  # generate VAPID keys
  #MASTODON_VAPI_KEY=$(podman run --rm -it tootsuite/mastodon bundle exec rake mastodon:webpush:generate_vapid_key)
  readarray -t MASTODON_VAPI_KEYS < <(podman run --rm -it tootsuite/mastodon bundle exec rake mastodon:webpush:generate_vapid_key)
  export VAPID_PRIVATE_KEY="$(echo -n ${MASTODON_VAPI_KEYS[0]})"
  export VAPID_PUBLIC_KEY="$(echo -n ${MASTODON_VAPI_KEYS[1]})"

}


mastodon_env() {
  
  # create a mastodon .env file
  # copy the generated secrets and keys into the .env file
  # make sure to set the LOCAL_DOMAIN as this cannot be changed later
  echo "
# This is a sample configuration file. You can generate your configuration
# with the `rake mastodon:setup` interactive setup wizard, but to customize
# your setup even further, you'll need to edit it manually. This sample does
# not demonstrate all available configuration options. Please look at
# https://docs.joinmastodon.org/admin/config/ for the full documentation.

# Note that this file accepts slightly different syntax depending on whether
# you are using `docker-compose` or not. In particular, if you use
# `docker-compose`, the value of each declared variable will be taken verbatim,
# including surrounding quotes.
# See: https://github.com/mastodon/mastodon/issues/16895

# Federation
# ----------
# This identifies your server and cannot be changed safely later
# ----------
LOCAL_DOMAIN=${WEB_DOMAIN}

# ----------
# Optional, if different than LOCAL_DOMAIN
# ----------
#WEB_DOMAIN=toots.webredirect.org

# Redis
# -----
REDIS_HOST=redis
REDIS_PORT=6379

# PostgreSQL
# ----------
DB_HOST=postgres
DB_USER=mastodon
DB_NAME=mastodon_production
DB_PASS=Ma5toD0n!Go7Hairy
DB_PORT=5432

# Secrets
# -------
# Make sure to use `rake secret` to generate secrets
# -------
SECRET_KEY_BASE=${MASTODON_SECRET_01}
OTP_SECRET=${MASTODON_SECRET_02}

# Web Push
# --------
# Generate with `rake mastodon:webpush:generate_vapid_key`
# --------
${VAPID_PRIVATE_KEY}
${VAPID_PUBLIC_KEY}

# Sending mail
# ------------
SMTP_SERVER=smtp.example.com
SMTP_PORT=25
SMTP_LOGIN=
SMTP_PASSWORD=
SMTP_FROM_ADDRESS=mastodon@example.com


# IP and session retention
# -----------------------
# Make sure to modify the scheduling of ip_cleanup_scheduler in config/sidekiq.yml
# to be less than daily if you lower IP_RETENTION_PERIOD below two days (172800).
# -----------------------
IP_RETENTION_PERIOD=31556952
SESSION_RETENTION_PERIOD=31556952" > /home/${USER_NAME}/mastodon/.env.mastodon

}


set_postgres_envs() {

  echo "
POSTGRES_USER=mastodon
POSTGRES_PASSWORD=Ma5toD0n!Go7Hairy
POSTGRES_DB=mastodon_production" > /home/${USER_NAME}/mastodon/.env.postgres
}


fire_up_containers() {
  # set owner of podman directory
  #chown -R "${USER_NAME}":"${USER_NAME}" /home/${USER_NAME}

  # create containers
  podman network create containers

  # run the postgesql container
  podman run -d --name postgres --env-file /home/${USER_NAME}/mastodon/.env.postgres  -v /home/${USER_NAME}/postgres:/var/lib/postgresql/data --network containers --restart=unless-stopped postgres:latest
  
  # run the redis container
  podman run -d --name redis -v /home/${USER_NAME}/redis:/data --network containers --restart=unless-stopped redis
  
  # initialize the mastodon database
  podman run --rm -it --network containers --env-file /home/${USER_NAME}/mastodon/.env.mastodon tootsuite/mastodon rails db:migrate
  
  # run the mastodon frontend container
  podman run -d --name mastodon --env-file /home/${USER_NAME}/mastodon/.env.mastodon -p 3000:3000 -v /home/${USER_NAME}/mastodon/public/system:/mastodon/public/system --network containers --restart=unless-stopped tootsuite/mastodon bash -c "rm -f /mastodon/tmp/pids/server.pid; bundle exec rails s -p 3000"
  
  # connect to shell inside mastodon conta
  export user_pwd="$(podman exec -it mastodon /bin/bash -c 'RAILS_ENV=production bin/tootctl accounts create '"'${USER01}'"' --email '"'${USER01_EMAIL}'"'  --confirmed')"
  export admin_pwd="$(podman exec -it mastodon /bin/bash -c 'RAILS_ENV=production bin/tootctl accounts create '"'${ADMIN01}'"' --email '"'${ADMIN01_EMAIL}'"' --confirmed')"

  podman exec -it mastodon /bin/bash -c "RAILS_ENV=production bin/tootctl accounts modify ${USER01} --email ${USER01_EMAIL} --role Owner"
  podman exec -it mastodon /bin/bash -c "RAILS_ENV=production bin/tootctl accounts modify ${ADMIN01} --email ${ADMIN01_EMAIL} --role Admin"

  # run the mastodon streaming container
  podman run -d --name mastodon-stream --env-file /home/${USER_NAME}/mastodon/.env.mastodon -p 4000:4000 --network containers --restart=unless-stopped tootsuite/mastodon node ./streaming
  
  # run the mastodon sidekiq container
  podman run -d --name mastodon-sidekiq --env-file /home/${USER_NAME}/mastodon/.env.mastodon --network containers -v /home/${USER_NAME}/mastodon/public/system:/mastodon/public/system --restart=unless-stopped tootsuite/mastodon bundle exec sidekiq


}


echo_pwds() {
  echo "das pwd fuer den user ${USER01_EMAIL} lautet:" $(echo ${user_pwd} | cut -d":" -f2)
  echo "das pwd fuer den user ${ADMIN01_EMAIL} lautet:" $(echo ${admin_pwd} | cut -d":" -f2)
}


gen_ssl_keys() {

  OPS=$(which openssl)
  CSRG="certs/wild_csr.csr"
  KEYG="certs/wild.key"
  CRTG="certs/wild.pem"
  DHKEY="certs/dhparam.pem"
  SUBJECT="/C=DE/ST=Hessen/L=Wiesbaden/O=HZD/OU=ITServices/CN=${WEB_DOMAIN}/emailAddress=${USER01_EMAIL}"
  
  mkdir -p certs
  
  # generate DH 2048
  ${OPS} dhparam -out ${DHKEY} 2048
  
  # gen key
  ${OPS} genpkey -algorithm RSA -pkeyopt rsa_keygen_bits:2048 -out ${KEYG}
  
  # remove passphrase
  ${OPS} rsa -in ${KEYG} -out ${KEYG}_nopwd
  
  # change rights
  chmod 600 ${KEYG}
  
  # gen csr
  ${OPS} req -new -sha512 -key ${KEYG} -out ${CSRG} -subj "${SUBJECT}"
  
  # sign key
  ${OPS} x509 -req -sha512 -days 3350 -in ${CSRG} -signkey ${KEYG} -out ${CRTG}
  
  # do some cleanup
  rm ${CSRG}
  rm ${KEYG}
  mv ${KEYG}_nopwd ${KEYG}

  # move certs to user dir
  mv certs /home/${USER_NAME}/
}


setup_n_start_nginx() {

  # download the default mastodon nginx configuration
  #wget -O /home/${USER_NAME}/nginx/conf/mastodon.conf https://raw.githubusercontent.com/mastodon/mastodon/main/dist/nginx.conf
  # or copy the local one
  cp nginx.conf /home/${USER_NAME}/nginx/conf/mastodon.conf

  # replace some options to work running in podman containers
  sed -i "s/try_files \$uri =404;/try_files \$uri @proxy;/" "/home/${USER_NAME}/nginx/conf/mastodon.conf"
  
  # update the server_name with the URL being used to reach mastodon
  # make sure to replace WEB_DOMAIN
  sed -i "s/server_name example.com;/\server_name ${WEB_DOMAIN};/" "/home/${USER_NAME}/nginx/conf/mastodon.conf"
  
  # update mastodon frontend server
  sed -i "s/server 127.0.0.1:3000/server mastodon:3000/" "/home/${USER_NAME}/nginx/conf/mastodon.conf"
  
  # update mastodon stream server
  sed -i "s/server 127.0.0.1:4000/server mastodon-stream:4000/" "/home/${USER_NAME}/nginx/conf/mastodon.conf"
  

  # for letsencrypt

  # update the ssl certificate path
  # make sure to replace DNS NAME
  #sed -i "s/# ssl_certificate\s*\/etc\/letsencrypt\/live\/example.com\/fullchain.pem;/ssl_certificate\t\/etc\/letsencrypt\/live\/<%DNS NAME%>\/fullchain.pem;/" "/home/${USER_NAME}/nginx/conf/mastodon.conf"
  
  # update the ssl key path
  # make sure to replace DNS NAME
  #sed -i "s/# ssl_certificate_key\s*\/etc\/letsencrypt\/live\/example.com\/privkey.pem;/ssl_certificate_key\t\/etc\/letsencrypt\/live\/<%DNS NAME%>\/privkey.pem;/" "/home/${USER_NAME}/nginx/conf/mastodon.conf"
  

  # set username in nginx.conf file
  sed -i "s/USER_NAME/${USER_NAME}/g" "/home/${USER_NAME}/nginx/conf/mastodon.conf"

  # set log-file name
  sed -i "s/XXXX/${NGINX_LOG_FILE}/g" "/home/${USER_NAME}/nginx/conf/mastodon.conf"

  
  
  # create nginx proxy container
  
  # self signed
  podman run --name nginx -p 80:80 -p 443:443 --network containers -v /home/${USER_NAME}/nginx/conf:/etc/nginx/conf.d:ro -v /home/${USER_NAME}/nginx/logs:/var/log/nginx:rw -v /home/${USER_NAME}/certs:/etc/nginx/certs:ro -d nginx
  
  # letsencrypt
  #podman run --name nginx -p 80:80 -p 443:443 --network containers -v /home/${USER_NAME}/nginx/conf:/etc/nginx/conf.d:ro -v /etc/letsencrypt:/etc/letsencrypt:ro -d nginx

}


# functions go here
install_podman
create_mastodon_user
fs_hierarchy
gen_ssl_keys
mastodon_env
set_postgres_envs
fire_up_containers
setup_n_start_nginx
echo_pwds